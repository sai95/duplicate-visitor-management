import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { FilterPipe } from '../app/components/country-code/Filter.pipe';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { FilePath } from '@ionic-native/file-path/ngx';
// import { FileTransferObject, FileTransfer } from '@ionic-native/file-transfer/ngx';
import { TokenInterceptor } from './auth/token.interceptor';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web'
import { RentalsSelectOrganizationComponent } from './components/rentals-select-organization/rentals-select-organization.component';


export function playerFactory(): any {  
  return import('lottie-web');
}
@NgModule({
  declarations: [
    AppComponent,
    RentalsSelectOrganizationComponent,
    FilterPipe],
  imports: [
    BrowserModule,
    LottieModule.forRoot({ player: playerFactory}),
    IonicModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    HttpClientModule],

  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor, multi: true
    }],
  bootstrap: [AppComponent],
})
export class AppModule { }
