import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { from, mergeMap, Observable } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    let token = window.localStorage.getItem('token');

    if (token&&!request.url.includes('s3')) {
      request = this.addToken(request, token);
    } else {
      request = this.organizations(request);
    }
    return next.handle(request)
  }

  private addToken(request: HttpRequest<any>, token: string) {

    return request.clone({
      setHeaders: {
        'Authorization': token,
        'Content-Type': 'application/json'
      }
    });
  }

  private organizations(request: HttpRequest<any>) {

    const organization = window.localStorage.getItem('organization') || ''; 

    return request.clone({
      setHeaders: {
        'Content-Type': 'application/json',
        'x-organizations': organization,
        'x-type': 'myTeam'
      }
    });
  }
}
