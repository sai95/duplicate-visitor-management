import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { LoadingController, ModalController, PopoverController } from '@ionic/angular';
import { CountryCodeComponent } from 'src/app/components/country-code/country-code.component';
import { AlertService } from 'src/app/services/alert.service';
import { LoginService } from '../../services/login.service';
import { LoadingService } from 'src/app/services/loading.service';
import * as _ from 'lodash';
import { StorageService } from 'src/app/services/storage.service';
import { Router } from '@angular/router';
import { ColorService } from 'src/app/services/color/color.service';
import { RentalsSelectOrganizationComponent } from 'src/app/components/rentals-select-organization/rentals-select-organization.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginVia: string = 'phone';
  public inputLabelText: string = 'Enter Phone Number';
  public allowedUsers = ['gatekeeper', 'security', 'employee'];
  public eventCopy: any;
  public showOtpCounter = false;
  public timeLeft = 60;
  public interval: any;
  public loginData: any = {
    phoneNumber: "",
    name: "",
    countryCode: "+91",
    loginType: 'login',
    accessCode: '',
    accessCode1: '',
    accessCode2: '',
    accessCode3: '',
    accessCode4: '',
    type:"otp-based-login"
  }
  public visibleBlock = 'phoneInput';
  constructor(
    private location: Location,
    private modalCtrl: ModalController,
    private alertService: AlertService,
    private loading: LoadingService,
    private loginService: LoginService,
    private storageService: StorageService,
    private router: Router,
    private colorService:ColorService,
    private popover: PopoverController,
  ) { }

  ngOnInit() {
  }
  goBack() {
    this.location.back();

  }
  selectSource() {
    this.inputLabelText = this.loginVia === 'phone' ? 'Enter Phone Number' : 'Enter Email';
    console.log(this.loginVia)
  }
  async showCountryCodeModal() {
    await this.modalCtrl.create({
      component: CountryCodeComponent,
      cssClass: 'my-custom-modal-css',
      componentProps: { value: this.loginData.countryCode }
    }).then(modal => {
      modal.present();
      modal.onDidDismiss().then((data: any) => {
        this.loginData.countryCode = data.data ? data.data : '+91';
        // console.log(data.data, "Data from country code modal");
      });
    });


  }
  startTimer() {
    this.timeLeft = 60;
    this.showOtpCounter = true;
    this.interval = setInterval(() => {
      if (this.timeLeft == 0) {
        this.showOtpCounter = false;
        clearInterval(this.interval);
      } else {
        this.timeLeft--;
      }
    }, 1000);
  }
  needHelp() {

  }

  handleSubmit() {

    window.localStorage.clear();

    if (!this.verifyPhone()) {
      this.alertService.presentAlert('', 'Please enter a valid phone number');
    } else {
      this.commonAuthService();
      // this.verifyPhoneService();
    }
  }

  verifyPhone() {
    const phoneno = /^[6-9]\d{9}$/;
    if (this.loginData.phoneNumber) {
      window.localStorage.setItem("phoneNumber", this.loginData.phoneNumber);
      window.localStorage.setItem("countryCode", this.loginData.countryCode);
      if (this.loginData.countryCode === '+91') {
        return this.loginData.phoneNumber.match(phoneno) ? true : false;
      } else {
        return this.loginData.phoneNumber.length > 4 ? true : false;
      }
    } else {
      return false;
    }

  }

  isUserAllowed(types: any) {
    return (_.intersection(this.allowedUsers, types).length > 0 ? true : false);
  }

  async verifyPhoneService() {
    if (this.verifyPhone()) {
      await this.loading.presentLoading();
      this.loginService.verifyPhone(this.loginData).subscribe(async (data: any) => {
        this.loading.dismiss();
        if (this.isUserAllowed(data.types)) {
          this.visibleBlock = 'otpInput';
          this.startTimer();
        } else {
          this.alertService.presentAlert('', 'You must be a gatekeeper to use this app');
        }

      }, async (err: any) => {
        console.log("err", err)
        await this.loading.dismiss();
        if (err.error.message.includes('User not found')) {
          this.alertService.presentAlert('', `User not found with ${this.loginData.countryCode} ${this.loginData.phoneNumber}`)
        } else {
          this.alertService.presentAlert('', 'Something went wrong');
        }
      })
    } else {
      this.alertService.presentAlert('', 'Please enter a valid phone number');
    }
  }

  async commonAuthService() {
    this.loading.presentLoading();

    this.loginService.commonAuth(this.loginData).subscribe(
      (data: any) => {

        this.loading.dismiss();

        if(data.organizations.length > 1) {
          this.presentOrganizationSelectionModal(data);
        }else{

          window.localStorage.setItem('baseURL',data.organizations[0].baseURL);

          window.localStorage.setItem('organization', data.organizations[0].organizationId);


          if (this.isUserAllowed(data.types)) {

            this.visibleBlock = 'otpInput';
            this.startTimer();

          } else {
            this.alertService.presentAlert('', 'You must be a gatekeeper to use this app');
          }
        }
      },
      (err) => {
        this.loading.dismiss();
        console.log("Error", err);
        this.alertService.presentAlert(
          "",
          err.error.message || "Something went wrong"
        );
      }
    );
  }

  async presentOrganizationSelectionModal (data: any) {
    this.popover
      .create({
        component: RentalsSelectOrganizationComponent,
        componentProps: { data: data },
        mode: "md",
        cssClass: "select-org-popover",
      })
      .then((d) => {
        d.present();
        d.onDidDismiss().then(async (data: any) => {

          if (data && data.data) {
            this.loginData.organizationId = data.data.organizationId;

            window.localStorage.setItem('baseURL',data.data.baseURL);

            window.localStorage.setItem('organization', data.data.organizationId);

            this.verifyPhoneService();
          }
        });
      });
  }

  next(el: any, prev: any, value: any) {
    this.eventCopy = event;
    if (value) {
      console.log('contains');
    } else {
      console.log('empty');
    }
    if (this.eventCopy.key == 'Backspace' && !value) {
      if (prev) {
        prev.setFocus();
      }
    } else if (this.eventCopy.key == 'Backspace' && value) {
    } else {
      el.setFocus();
    }
  }

  async setValues(data: any) {
    window.localStorage.setItem('isLoggedIn', 'true');
    window.localStorage.setItem('userId', data.uid);
    window.localStorage.setItem('token', data.token);
    window.localStorage.setItem('organization', data.organization);
    window.localStorage.setItem('userRole', data.role?._id);
    window.localStorage.setItem('organizationTimezone', data.organizationTimezone ? data.organizationTimezone : Intl.DateTimeFormat().resolvedOptions().timeZone);
    if(data?.visitorManagementAppTheme){
      window.localStorage.setItem('primaryColor', data.visitorManagementAppTheme?.primaryColor || "#ffbc00" );
    }
    if(data.visitorManagementAppTheme){
      window.localStorage.setItem('secondaryColor', data.visitorManagementAppTheme?.secondaryColor || "#7e1946");
    }
    window.localStorage.setItem('orgLogo', data.visitorManagementAppTheme?.logo);

    this.colorService.setPrimaryColor();
    this.colorService.setSecondaryColor();
    this.router.navigate(['/home'])
  }

  public backButtonCase(): void {
    switch (this.visibleBlock) {
      case 'otpInput':
        this.visibleBlock = 'phoneInput';
        break;
      case 'phoneInput':
        break;
      default:
        this.visibleBlock = 'phoneInput';
        break;

    }
  }

  async login() {
    await this.loading.presentLoading();
    console.log(this.loginData)
    if (this.loginData.accessCode1 && this.loginData.accessCode2 && this.loginData.accessCode3 && this.loginData.accessCode4) {
      this.loginData.accessCode = this.loginData.accessCode1 + '' + this.loginData.accessCode2 + '' + this.loginData.accessCode3 + '' + this.loginData.accessCode4;
    }
    this.loginService.login(this.loginData).subscribe(async (data: any) => {
      setTimeout(() => {
        this.loading.dismiss();
      }, 500);
      this.setValues(data);
    }, (err) => {
      this.alertService.presentAlert('', err.error.message);
      setTimeout(() => {
        this.loading.dismiss();
      }, 500);
      console.log("Error")
    })
    // this.loginService.login(this.loginData)
    //   .subscribe(async (data: any) => {
    //     setTimeout(() => {
    //       this.loading.dismiss();
    //     }, 200);
    //     this.setValues(data);
    //   },
    //     async err => {
    //       this.alertService.presentAlert('', err.error.message);
    //       setTimeout(() => {
    //         this.loading.dismiss();
    //       }, 200);
    //     }
    //   );
  }
}
