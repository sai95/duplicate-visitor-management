import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor(
    private loadingController: LoadingController,
  ) { }

  async presentLoading() {
    await this.loadingController.create({
      spinner: 'lines'
    }).then(loading => {
      loading.present();
    });

  }

  async dismiss(){
    await this.loadingController.dismiss();
  }
}
