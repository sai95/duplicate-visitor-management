import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config, getApi } from 'src/app/config';
@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  public uploadFileToS3Server(file: string, URl: string, mimeType: string = 'image/jpeg'): Observable<any> {
    const data = this.DataURIToBlob(file);
    console.log('DataURIToBlob');
    console.log(data);
    const formData = new FormData();
    formData.append('file', data, 'fileNameTest.jpeg');
    return this.httpClient.put(URl, formData.get('file'), { headers: new HttpHeaders({ 'Content-Type': mimeType }) })
  }


  getPutSignedUrl(fileName: string, shared: boolean = false): Observable<any> {
    return this.httpClient.post(`${getApi()}/api/document/presigned`, { fileName: fileName, shared: shared })
  }

  private DataURIToBlob(dataURI: string): Blob {
    const splitDataURI = dataURI.split(',')
    const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
    const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

    const ia = new Uint8Array(byteString.length)
    for (let i = 0; i < byteString.length; i++)
      ia[i] = byteString.charCodeAt(i);

    return new Blob([ia], { type: mimeString })
  }

}
