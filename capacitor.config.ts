import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.thehousemonk.visitormanagement',
  appName: 'Visitor Management for RE',
  webDir: 'www',
  server: {
    // url: "http://0.0.0.0:4200",
    cleartext: true
  },
  plugins: {
    SplashScreen: {
      launchShowDuration: 3000,
      launchAutoHide: true,
      launchFadeOutDuration: 200,
      androidSplashResourceName: "splash",
      androidScaleType: "CENTER_CROP",
      showSpinner: true,
      androidSpinnerStyle: "large",
      iosSpinnerStyle: "small",
      spinnerColor: "#999999",
      splashFullScreen: true,
      splashImmersive: true,
      layoutName: "launch_screen",
      useDialog: true,
    },
    plugins: {
      LiveUpdates: {
        appId: '743d4b9a',
        channel: 'Production',
        autoUpdateMethod: 'background',
        maxVersions: 2
      }
    }
  },

};

export default config;
