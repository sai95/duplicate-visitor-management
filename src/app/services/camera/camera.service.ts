import { Injectable } from '@angular/core';
import { CameraPreview, CameraPreviewOptions, CameraPreviewPictureOptions } from '@capacitor-community/camera-preview';
// import { Camera, CameraDirection, CameraResultType, CameraSource } from '@capacitor/camera';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CameraService {
  public imageUrl: any;
  constructor(
    private platform: Platform,
  ) { }




  public startCamera(): Promise<{}> {
    const height = (document.getElementById('topHeader')?.clientHeight || 400) + (document.getElementById('topHeader')?.getBoundingClientRect().top || 292) + 30;
    const cameraPreviewOptions: CameraPreviewOptions = {
      disableAudio: true,
      width: Math.floor(this.platform.width() * 0.5),
      height: Math.floor(this.platform.width() * 0.5),
      position: 'front',
      x: Math.floor(Math.floor(this.platform.width() * .50) - Math.floor(this.platform.width() * .25)),
      y: Math.floor(height),
      parent: "cameraPreview",
      disableExifHeaderStripping: false,
      lockAndroidOrientation: true,
      rotateWhenOrientationChanged: true
    }

    return CameraPreview.start(cameraPreviewOptions);

  }

  public stopCamera(): Promise<{}> {
    return CameraPreview.stop();
  }

  public async captureImage(): Promise<{ value: string; }> {
    const cameraPreviewPictureOptions: CameraPreviewPictureOptions = {
      width: Math.floor(this.platform.width() * 0.5),
      height: Math.floor(this.platform.width() * 0.5),
      quality: 100
    }

    return CameraPreview.capture(cameraPreviewPictureOptions);
  }

}
