import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  constructor() { }

  setPrimaryColor() {
    const  PRIMARY_COLOR = window.localStorage.getItem('primaryColor')||'#ffbc00';
    if (PRIMARY_COLOR != '') {
       document.documentElement.style.setProperty('--ion-color-primary', PRIMARY_COLOR);
       document.documentElement.style.setProperty('--ion-color-primary-shade', PRIMARY_COLOR);
       document.documentElement.style.setProperty('--ion-color-primary-tint', PRIMARY_COLOR);
    }
  }

  setSecondaryColor() {
    const SECONDARY_COLOR = window.localStorage.getItem('secondaryColor')||'#7e1946';
    if (SECONDARY_COLOR != '') {
       document.documentElement.style.setProperty('--ion-color-secondary', SECONDARY_COLOR);
       document.documentElement.style.setProperty('--ion-color-secondary-shade', SECONDARY_COLOR);
       document.documentElement.style.setProperty('--ion-color-secondary-tint', SECONDARY_COLOR);
    }
  }
}
