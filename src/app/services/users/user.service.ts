import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config, getApi } from 'src/app/config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  getUsers(searchString: any): Observable<any> {
    return this.http.get(`${getApi()}/api/user/visitor-search-with-organization?q=${searchString}&limit=40`);
  }

  getUserById(userId: any): Observable<any> {
    return this.http.get(`${getApi()}/api/user/${userId}`);
  }
  
  updateUser(userData: any): Observable<any> {
    return this.http.put(`${getApi()}/api/user/${userData._id}`, userData);
  }
}