import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config, getApi } from 'src/app/config';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {

  constructor(
    private http: HttpClient
  ) { }

  getDeliveriesList(data: any): Observable<any> {
    return this.http.get(`${getApi()}/api/visitor-log/vendor-list`)
  }
}
