export interface visitorData {
  inTime?: string,
  type?: string,
  phoneNumber?: string,
  countryCode?: string,
  email?: string,
  tenant: string,
  name?: string,
  status: string,
  files: any,
  visitDate:string,
  home:string,
  vendor?: {
    id?: string,
    name?: string,
    logo?: string,
  }
  updateVisitor?:boolean
}