import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config , getApi } from '../config';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    public http: HttpClient,
  ) { }



  commonAuth(data: any): Observable<any> {
    return this.http.post(`${getApi()}/api/common-auth`, data);
  }

  verifyPhone(data: any): Observable<any> {
    return this.http.post(`${getApi()}/api/verify-phone`, data);
  }
  login(data: any): Observable<any> {
    return this.http.post(`${getApi()}/api/login-otp`, data);
  }
  newRentalsRefreshToken(): Observable<any> {
    return this.http.get(`${getApi()}/api/user/refresh-payload`);
  }
}
