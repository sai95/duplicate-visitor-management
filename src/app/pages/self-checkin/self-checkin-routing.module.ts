import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelfCheckinPage } from './self-checkin.page';

const routes: Routes = [
  {
    path: '',
    component: SelfCheckinPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelfCheckinPageRoutingModule {}
