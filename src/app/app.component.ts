import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ColorService } from './services/color/color.service';
import { StorageService } from './services/storage.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public isLoggedIn: any;
  constructor(
    private storageService: StorageService,
    private router: Router,
    private colorService: ColorService,
    private platform: Platform
  ) {
    this.platform.ready().then(() => {
      this.initializeApp();
    })
  }

  async initializeApp() {

    this.isLoggedIn = await window.localStorage.getItem('isLoggedIn');
    this.isLoggedIn == 'true' ? this.router.navigate(['/home']) : this.router.navigate(['/login']);
    this.colorService.setPrimaryColor();
    this.colorService.setSecondaryColor();
    console.log(this.isLoggedIn)

  }
}
