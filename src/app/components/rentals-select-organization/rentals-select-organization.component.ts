import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-rentals-select-organization',
  templateUrl: './rentals-select-organization.component.html',
  styleUrls: ['./rentals-select-organization.component.scss'],
})
export class RentalsSelectOrganizationComponent  implements OnInit {
  
  @Input() data: any;

  constructor(
    public popOver: PopoverController
  ) { }

  ngOnInit(
    
  ) {}

  async close(data?: any) {
    if (data) {
      this.popOver.dismiss(data)

    } else {
      this.popOver.dismiss()
    }
  }

}
