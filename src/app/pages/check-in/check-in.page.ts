import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Keyboard } from '@capacitor/keyboard';
import { IonInput, Platform } from '@ionic/angular';
import { CameraService } from 'src/app/services/camera/camera.service';
import { FileService } from 'src/app/services/file/file.service';
import { VisitorService } from 'src/app/services/visitors/visitor.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertService } from 'src/app/services/alert.service';
import * as moment from 'moment';
import 'moment-timezone';

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.page.html',
  styleUrls: ['./check-in.page.scss'],
})
export class CheckInPage implements OnInit, OnDestroy {
  @ViewChild('autofocus', { static: false }) input1!: IonInput;
  public displayPictureContainer = {
    number: 0,
  }
  public orgLogo: any;
  constructor(
    private router: Router,
    private platform: Platform,
    private cameraService: CameraService,
    private fileService: FileService,
    private visitorService: VisitorService,
    private loadingService: LoadingService,
    private alertService: AlertService
  ) {
    this.orgLogo = window.localStorage.getItem('orgLogo');
   }

  // ngOnInit() {
  // }
  public date = new Date().toISOString();
  public loginData: any = {
    accessCode1: '',
    accessCode2: '',
    accessCode3: '',
    accessCode4: '',
    accessCode5: '',
    accessCode6: '',

  };
  public visitorData = {
    name: "",
    phoneNumber: "",
    email: "",
    tenant: {
      name: "",
      company: "",
      time: '',
      QRCode: "",
      purpose: ""
    },
  }
  public visitorUpdatePayload: any = {
    files: [],
    id: "",
    updateVisitor:true
  }

 
  public eventCopy: any;
  public route = true;
  public showImage: boolean = false;
  public showProgress: boolean = true;
  public step = 1;
  public allAccessCodeExist: boolean = false;
  public buttonText: string = 'Next';
  public hideButton: boolean = false;
  public timeLeft: number = 5;
  public imageResult: any;
  public capturedImage: any;
  public Date  = new Date().toISOString();
  public capturingHeight: number = 0;
  public intervalTimer: any;

  ngOnInit() {
    setTimeout(() => {
      this.displayPictureContainer.number = Math.floor(this.platform.width() * 0.5);
    }, 500);
    setInterval(()=>{
      this.Date  =  new Date().toISOString();
    }, 1000);
  }

  ngOnDestroy(): void {
    this.cameraService.stopCamera();
    clearInterval(this.intervalTimer);
  }

  ionViewWillEnter() {
    setTimeout(() => this.input1.setFocus(), 500);
  }

  public addOnFocusClass(id: string) {
    document.getElementById(id)?.classList.add('otp-focus-border');
  }

  public removeOnFocusClass(id: string) {
    document.getElementById(id)?.classList.remove('otp-focus-border');
  
  }
  next(el: any, prev: any, value: any) {
    this.eventCopy = event;
    this.handleChange(this.eventCopy);
    this.checkAccessCodes();
    if (this.eventCopy.key == 'Backspace' && !value) {
      if (prev) {
        prev.setFocus();
      }
    } else if (this.eventCopy.key == 'Backspace' && value) {
    } else {
      el.setFocus();
    }
  }

  async handleChange(event: any) {
    if (this.checkAccessCodes()) {
      Keyboard.hide();
      this.showImage = true;
      // this.loginData.accessCode = `${this.loginData.accessCode1}${this.loginData.accessCode2}${this.loginData.accessCode3}${this.loginData.accessCode4}${this.loginData.accessCode5}${this.loginData.accessCode6}`;
    } else {
      this.showImage = false;
    }
  }

  checkAccessCodes() {
    const loginDataKeys = Object.keys(this.loginData);
    this.allAccessCodeExist = loginDataKeys.every(key => this.loginData[key] !== '');
    if (this.allAccessCodeExist == true) {
      return true;
    } else {
      return false;
    }
  }

  async startTimer(returnBack = '') {
    await this.cameraService.startCamera();
    this.intervalTimer = setInterval(async () => {
      if (this.timeLeft === 0) {
        this.hideButton = false;
        clearInterval(this.intervalTimer);
      } else {
        if (this.timeLeft === 1) {
          this.capturedImage = `data:image/png;base64,${(await this.cameraService.captureImage()).value || ''}`;
          this.addPictureToUserData();
          await this.cameraService.stopCamera();
          try {
            await this.cameraService.stopCamera();
          } catch (error) { }
        }
        this.timeLeft--;
      }
    }, 1000);
  }

  private addPictureToUserData() {
    this.fileService.getPutSignedUrl('visitorProfile.jpeg').subscribe((signedURLData: any) => {

      this.fileService.uploadFileToS3Server(this.capturedImage, signedURLData.url).subscribe(data => {
        this.visitorUpdatePayload.files.push(signedURLData);
      })
    })
  }

  routeToHomePage() {
    this.intervalTimer = setInterval(() => {
      console.log("time",)
      if (this.timeLeft === 0) {
        this.router.navigate(['/homepage'])
        clearInterval(this.intervalTimer)
      }
      this.timeLeft--;
    }, 1000)
  }


  tryAgain() {
    this.step = 2;
    this.timeLeft = 5;
    this.hideButton = true;
    this.visitorUpdatePayload.files = [];
    setTimeout(() => {
      this.startTimer();
    }, 500);
  }


  async getVisitorWithOTP() {
    await this.loadingService.presentLoading();
    const accessCodes = Object.values(this.loginData).join('');
    this.visitorService.getVisitorWithOTP(accessCodes).subscribe((data: any) => {
      try {
        if ((Object.entries(data).length) !== 0) {
          const { visitorLog: { name, phoneNumber, email, gatePass } } = data;
          this.step++
          this.hideButton = true;
          this.showProgress = false;
          this.visitorUpdatePayload.id = data.visitorLog._id;
          setTimeout(() => {
            this.capturingHeight = Math.floor(Math.floor(this.platform.width() * 0.5) + Math.floor(document.getElementById('topHeader')?.clientHeight || 400) + Math.floor(document.getElementById('topHeader')?.getBoundingClientRect().top || 292) + 40);
            this.startTimer();
          }, 500);
          console.log(this.step, this.hideButton, this.capturingHeight);
          this.buttonText = 'Continue';
          console.log('GOT THE DATA', data)
          this.visitorData.name = name;
          this.visitorData.phoneNumber = phoneNumber;
          this.visitorData.email = email;
          this.visitorData.tenant.QRCode = `data:image/png;base64,${gatePass?.QRCode}`;
          this.visitorData.tenant.purpose = gatePass.purpose;
          this.visitorData.tenant.time = gatePass.issuedDate;
          if (data?.visitorLog?.tenant) {
            this.visitorData.tenant.name = data?.visitorLog?.tenant?.firstName;
          }
          // update payload
        
        } else {
          this.step = 1;
          this.loginData.accessCode1 = '',
            this.loginData.accessCode2 = '',
            this.loginData.accessCode3 = '',
            this.loginData.accessCode4 = '',
            this.loginData.accessCode5 = '',
            this.loginData.accessCode6 = '',
            this.alertService.presentAlert('', 'User not found!');
          this.showImage = false;
          this.checkAccessCodes();
        }
      } catch (error) {
        // this.alertService.presentAlert('', 'User not found!');
      }
      setTimeout(async () => {
        await this.loadingService.dismiss();
      }, 500)
    }, (err: any) => {
      this.loadingService.dismiss();
      this.alertService.presentAlert('', err.error.message ?  err.error.message : "Something went wrong" )
      this.step = 1;
      console.log("Error", err)
    })
  }

  nextStep() {
    switch (this.step) {
      case 1:
        console.log("log1")
        this.getVisitorWithOTP();
        break;
      case 2:
        console.log("log2")
        this.showProgress = true;
        this.createGatePass();
        break;
      default:
      // code block
    }

  }
  
  previousStep(event: MouseEvent,step:number) {
    const clickedElement = event.target as HTMLElement;
    if (clickedElement.classList.contains('active')) {
    if (step > 0) {
      switch (this.step) {
        case 1:
          break;
        case 2:
          this.step--
          this.timeLeft = 5;
          this.hideButton = false;
          this.cameraService.stopCamera();
          clearInterval(this.intervalTimer);
          break;
        default:
        // code block
      }
    }
    }
  }

  createGatePass() {
    this.loadingService.presentLoading();
    this.visitorService.updateVisitor(this.visitorUpdatePayload).subscribe((data: any) => {
    this.hideButton = true;
    console.log(this.visitorUpdatePayload);
    setTimeout(() => {
      this.loadingService.dismiss();
      this.step++
      console.log("visitor updated")
    }, 500)
    this.timeLeft = 5;
    this.routeToHomePage();
    }, (err) => {
      this.loadingService.dismiss();
      this.alertService.presentAlert('', 'something went wrong')
      console.log("error", err)
    })
  }
}
