import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonInput, ModalController, Platform } from '@ionic/angular';
import { CountryCodeComponent } from 'src/app/components/country-code/country-code.component';
import { AlertService } from 'src/app/services/alert.service';
import { DeliveryService } from 'src/app/services/delivery/delivery.service';
import { LoadingService } from 'src/app/services/loading.service';
import { UserService } from 'src/app/services/users/user.service';
import { userList } from 'src/app/interface/user-data';
import { VisitorService } from 'src/app/services/visitors/visitor.service';
import { CameraService } from 'src/app/services/camera/camera.service';
import { FileService } from 'src/app/services/file/file.service';
import { visitorData } from 'src/app/interface/visitor-data';
import { isEmpty } from 'lodash';
import * as moment from 'moment';
import 'moment-timezone';
import { AnimationOptions } from 'ngx-lottie';
import { AnimationItem } from 'lottie-web';


@Component({
  selector: 'app-self-checkin',
  templateUrl: './self-checkin.page.html',
  styleUrls: ['./self-checkin.page.scss'],
})
export class SelfCheckinPage implements OnInit, OnDestroy {
  @ViewChild('autofocus', { static: false }) phoneNumberInput!: IonInput;
  public step: number = 1;
  public source: string = 'phoneNumber';
  public showProgress: boolean = true;
  public hideButton: boolean = false;
  public selectIndex: number | undefined;
  public spinner: boolean = false;
  public QRCode: string = ''
  public capturingHeight: number = 0;
  public intervalTimer: any;
  public punchCircleTop: number = 0;
  public punchCircleRight: number = 0;
  public punchCircleLeft: number = 0;
  public displayPictureContainer = {
    number: 0,
  }
  public orgLogo: any;
  // public labelText: string = 'Enter Phone Number';
  public user: any = {
    email: '',
    countryCode: "+91",
    phoneNumber: '',
    type: 'guest',
    visitDate:  new Date().toISOString(),
    createdBy: '',
    name: '',
    files: []
  }
  public approvalTimeOut: boolean = false;
  public visibleBlock: string = 'user-details';
  public userList: Array<userList> = [];
  public deliveryList: { name: string, logo: string }[] = [];
  public filteredUserList: any = [];
  public searchText: string = ''
  public minSearchLength: number = 3;
  public timeLeft: number = 5;
  public visitorType: string = '';
  public deliveryData = {
    region: 'bengaluru',
    type: 'food-delivery'

  }
  public selectedUser: any;
  public isEnable: boolean = false;
  public isChecked: boolean = true;
  public buttonText: string = 'Next';
  public otherDelivery: string = '';
  public approvalStatus: string = 'pending';
  public visitorData: visitorData = {
    status: 'pending-approval',
    type: "",
    phoneNumber: "",
    email: "",
    tenant: "",
    name: "",
    files: [],
    vendor: {
      id: "",
      name: "",
      logo: ""
    },
    countryCode: '',
    home: '',
    visitDate: new Date().toISOString()
  };
  public capturedImage: any;
  public croppedFaceBase64: string | null = null;
  public emptyUserList: boolean = false;
  public purpose: string = '';
  public options: AnimationOptions = {
    path: '/assets/loader/loader.json',
    
  };
  private animationItem: AnimationItem | undefined;
  constructor(
    private modalCtrl: ModalController,
    private router: Router,
    private route: ActivatedRoute,
    private loading: LoadingService,
    private alertService: AlertService,
    private userService: UserService,
    private deliveryServices: DeliveryService,
    private visitorService: VisitorService,
    private cameraService: CameraService,
    private fileService: FileService,
    private platform: Platform
  ) {
    this.orgLogo = window.localStorage.getItem('orgLogo');
  }
  ionViewWillEnter() {
    setTimeout(() => this.phoneNumberInput.setFocus(), 500);
  }
  ngOnInit() {
    this.route.queryParamMap.subscribe((params: any) => {
      this.visitorType = params.params.visitorType;
    });
    setTimeout(() => {
      this.displayPictureContainer.number = Math.floor(this.platform.width() * 0.5);
      this.punchCircleTop = Math.floor(document.getElementById('dashed-border')?.offsetHeight || 0);
      // this.punchCircleRight = Math.floor(document.getElementById('dashed-border')?.getBoundingClientRect().left || 572) - 10;
    }, 500);
    if (this.visitorType === 'delivery') {
      this.getDeliveriesList();
    }
  }

  ngOnDestroy(): void {
    this.cameraService.stopCamera();
    clearInterval(this.intervalTimer);
    this.animationItem?.pause();
  }

  onAnimate(animationItem: AnimationItem): void {    
    console.log(animationItem);  
  }
  
  async startTimer() {
    await this.cameraService.startCamera();
    this.intervalTimer = setInterval(async () => {
      if (this.timeLeft === 0) {
        this.hideButton = false;
        clearInterval(this.intervalTimer);
      } else {
        if (this.timeLeft === 1) {
          this.capturedImage = `data:image/png;base64,${(await this.cameraService.captureImage()).value || ''}`;
          this.addPictureToUserData();
          await this.cameraService.stopCamera();
          try {
            await this.cameraService.stopCamera();
          } catch (error) { }
        }
        this.timeLeft--;
      }
    }, 1000);
  }

  routeToHomePage() {
    this.intervalTimer = setInterval(() => {
      if (this.timeLeft === 0) {
        this.router.navigate(['/homepage'])
        clearInterval(this.intervalTimer)
      }
      this.timeLeft--;
    }, 1000)
  }

  tryAgain() {
    this.step = 3;
    this.timeLeft = 5;
    this.hideButton = true;
    this.capturedImage = null;
    this.user.files = [];
    setTimeout(() => {
      this.startTimer();
    }, 500);
  }

  private addPictureToUserData() {
    this.fileService.getPutSignedUrl('visitorProfile.jpeg').subscribe((signedURLData: any) => {

      this.fileService.uploadFileToS3Server(this.capturedImage, signedURLData.url).subscribe(data => {
        this.user.files = [signedURLData]
      })
    })
  }


  selectSource() {
    console.log(this.source, this.isEnable)
    this.isEnable = false;
    if (this.source == 'phoneNumber') {
      this.user.email = '';
      this.user.name = '';
    } else {
      this.user.phoneNumber = '';
      this.user.name = '';
    }
  }

  async filterItems() {
    if (this.searchText.length >= this.minSearchLength) {
      await this.getUsers('');
    } else {
      this.isEnable = false;
      this.emptyUserList = false;
      this.filteredUserList = []; // Show all items if the search term is too short
    }
  }


  getDeliveriesList() {
    this.loading.presentLoading();
    this.deliveryServices.getDeliveriesList(this.deliveryData).subscribe((data: any) => {
      console.log("data", data)
      setTimeout(() => {
        this.loading.dismiss();
      }, 500)
      this.deliveryList = data;
    }, (err) => {
      this.loading.dismiss();
      this.alertService.presentAlert('', 'Error while getting deliveries')
      console.log("Error", err)
    })
  }

  filterList(searchTerm: string) {
    console.log('step3 ', this.userList)
    return this.userList.filter((item: any) =>
      item?.firstName?.toLowerCase().includes(searchTerm.toLowerCase())
    );
  }
  async showCountryCodeModal() {
    await this.modalCtrl.create({
      component: CountryCodeComponent,
      cssClass: 'my-custom-modal-css',
      componentProps: { 'value': this.user.countryCode }
    }).then((modal: { present: () => void; onDidDismiss: () => Promise<any>; }) => {
      modal.present();
      modal.onDidDismiss().then((data: any) => {
        this.user.countryCode = data.data ? data.data : this.user.countryCode;
        // console.log(data.data, "Data from country code modal");
      });
    })
  }


  selectUser(user: any) {
    this.selectedUser = user;
    this.isEnable = Object.keys(this.selectedUser).length ? true : false;
    console.log("Selected", this.selectedUser)
  }

  getUsers(event: any) {
    if (!event) {
      this.spinner = true
    }
    this.userService.getUsers(this.searchText).subscribe((data: any) => {
      this.userList = data?.rows;
      console.log("first", this.filteredUserList)
      this.filteredUserList = this.filterList(this.searchText);
      event ? event.target?.complete() : this.spinner = false;
      this.emptyUserList = this.filteredUserList.length <= 0 ? true : false;
      console.log("sec", this.filteredUserList)
    }, (err) => {
      this.spinner = false;
      this.alertService.presentAlert('', 'something went wrong')
      console.log("Error", err)
    })
  }

  selectDelivery(delivery: any, index: number) {
    this.selectIndex = index;
    this.isEnable = Object.keys(delivery).length ? true : false;
    if (this.visitorData.vendor) {
      this.visitorData.vendor.id = delivery._id;
      this.visitorData.vendor.name = delivery.name;
      this.visitorData.vendor.logo = delivery.logo;
    }
    console.log("visitorData", this.visitorData)
  }

  handleChange(event: Event) {
    this.isEnable = this.otherDelivery ? true : false;
    this.selectIndex  = undefined;
    this.visitorData.vendor = { name: this.otherDelivery };
  }

  checkValidation(value: any) {
    if (value == true && this.user.name != '' && this.user.phoneNumber != '') {
      this.isEnable = true;
    } else if (value == true && this.user.name != '' && this.user.email != '') {
      this.isEnable = true;
    } else {
      this.isEnable = false;
    }
  }
  IonNameChange() {
    const alphabeticPattern = /^[a-zA-Z\s]+$/;
    if (this.user.name != '' && alphabeticPattern.test(this.user.name)) {
      const validPhonePatten = this.validatePhone() || this.validateEmail() ? true : false;
      this.checkValidation(validPhonePatten)
    } else {
      this.checkValidation(false)
    }
  }

  validatePhone() {
    let isValidPhone;
    const phoneNumberPattern = /^[6-9]\d{9}$/;
    if (this.user.phoneNumber) {
      if (this.user.countryCode === '+91') {
        isValidPhone = this.user.phoneNumber.match(phoneNumberPattern) ? true : false;
        this.checkValidation(isValidPhone)
        return this.user.phoneNumber.match(phoneNumberPattern) ? true : false;
      } else {
        isValidPhone = this.user.phoneNumber.length > 4 ? true : false;
        this.checkValidation(isValidPhone)
        return this.user.phoneNumber.length > 4 ? true : false;
      }
    } else {
      this.checkValidation(false)
      return false;
    }
  }
  validateEmail() {
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    if (this.user.email) {
      const validEmail = emailRegex.test(this.user.email) ? true : false;
      this.checkValidation(validEmail);
      return validEmail
    } else {
      this.checkValidation(false);
      return false
    }
  }

  nextStep() {
    console.log("logg", this.step)
    switch (this.step) {
      case 1:
        console.log("step1", this.selectedUser)
        this.step++
        this.isEnable = false
        break;
      case 2:
        this.step++;
        this.hideButton = true;
        this.showProgress = false;
        console.log("step2")
        setTimeout(() => {
          this.capturingHeight = Math.floor(Math.floor(this.platform.width() * 0.5) + Math.floor(document.getElementById('topHeader')?.clientHeight || 400) + Math.floor(document.getElementById('topHeader')?.getBoundingClientRect().top || 292) + 40);
          this.startTimer();
        }, 500);
        break;
      case 3:
        this.createGatePass();
        break;
      default:
      // code block
    }
  }

  previousStep(event: MouseEvent,step:number) {
    const clickedElement = event.target as HTMLElement;
    if (clickedElement.classList.contains('active')) {
      // Handle your click functionality here
    this.hideButton = false;
    if (step > 0) {
      switch (this.step) {
        case 1:
          break;
        case 2:
          this.step--
          console.log("log12")
          this.isEnable = true;
          this.selectedUser = {};
          this.filteredUserList = [];
          this.searchText = '';
          break;
        case 3:
          console.log("log13")
          this.isEnable = true;
          this.cameraService.stopCamera();
          clearInterval(this.intervalTimer);
          this.timeLeft = 5;
          this.step--
          break;
        default:
        // code block
      }
    }
    }
  }

  createGatePass() {
    this.visitorData.type = this.visitorType;
    this.visitorData.phoneNumber = this.user.phoneNumber;
    this.visitorData.email = this.user.email;
    this.visitorData.tenant = this.selectedUser.tenantId;
    this.visitorData.name = this.user.name;
    this.visitorData.files = this.user.files;
    this.visitorData.countryCode = this.user.countryCode;
    this.visitorData.home = this.selectedUser.homeId;

    if (this.visitorType != 'delivery') {
      delete this.visitorData.vendor;
    } else if (this.visitorType == 'delivery') {
      this.visitorData.name = 'Delivery';
      delete this.visitorData.phoneNumber;
      delete this.visitorData.email;
      delete this.visitorData.countryCode;
    }
    console.log("=======", this.visitorData)
    this.loading.presentLoading();
    this.visitorService.createVisitor(this.visitorData).subscribe((data: any) => {
      this.approvalStatus = data?.status;
      if(data?.status === 'pending-approval' && (this.visitorType == 'guest' || this.visitorType == 'vendor')){
        this.timeLeft = 120;
        this.purpose = data?.gatePass?.purpose || 'Walk-in';
        this.checkApproval(data);
      }else{
        this.timeLeft = 5;
        this.purpose = data?.gatePass?.purpose || 'Delivery';
        this.routeToHomePage();
      }
      this.step++
      this.isEnable = false
      this.hideButton = true;
      this.showProgress = true;

      console.log("visitor created", data)
      this.loading.dismiss();
      this.punchCircleTop = document.getElementById('dashed-border')?.getBoundingClientRect().top || 433;
    }, (err) => {
      this.loading.dismiss();
      this.alertService.presentAlert('', err.error.message)
      console.log("ERROR", err)
    })
  }

  checkApproval(data: any = {}){
    this.intervalTimer = setInterval(() => {
      this.timeLeft--;

      // Check if it's time to call the API (every 5 seconds)
      if (this.timeLeft % 5 === 0) {
        this.getVisitor(data?._id);
      }

      // Check if the timer has reached 0
      if (this.timeLeft <= 0) {
        this.timeLeft = 30;
        this.approvalTimeOut = true;
        this.routeToHomePage();
        clearInterval(this.intervalTimer);
      }
    }, 1000);
  }

  getVisitor(id: any = ''){
    this.visitorService.getVisitor(id).subscribe((data: any)=>{
      this.approvalStatus = data?.status;
      if (data?.status === 'active') {
        this.QRCode = `data:image/png;base64,${data?.gatePass?.QRCode}`;
        clearInterval(this.intervalTimer);
        this.timeLeft = 5;
        this.routeToHomePage();
      }
    },(err)=>{
      console.log(err);
      this.alertService.presentAlert('', err.error.message || 'Something went wrong')
      clearInterval(this.intervalTimer);
    })
  }

}
