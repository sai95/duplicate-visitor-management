import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SelfCheckinPage } from './self-checkin.page';

describe('SelfCheckinPage', () => {
  let component: SelfCheckinPage;
  let fixture: ComponentFixture<SelfCheckinPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(SelfCheckinPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
