import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  public organizationTimezone: any ;
  constructor(
    private alertController: AlertController,
  ) { 
    this.organizationTimezone = window.localStorage.getItem('organizationTimezone');
  }

  async presentAlert(header: string, subheader: string) {
    const alert = await this.alertController.create({
      header,
      subHeader: subheader,
      buttons: ['OK']
    });
    await alert.present();
  }

  public getTimeZone(): string {
    return moment.tz(this.organizationTimezone).format('ZZ');
  }
}
