export interface userList {
    firstName?: string,
    lastName?: string,
    organizations?: string,
    _id?: string
} 