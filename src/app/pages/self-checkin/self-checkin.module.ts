import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelfCheckinPageRoutingModule } from './self-checkin-routing.module';

import { SelfCheckinPage } from './self-checkin.page';
import { LottieModule } from 'ngx-lottie';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelfCheckinPageRoutingModule,
    LottieModule
  ],
  declarations: [SelfCheckinPage]
})
export class SelfCheckinPageModule {}
