import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { AlertService } from "src/app/services/alert.service";
import { LoadingService } from "src/app/services/loading.service";
import { UserService } from "src/app/services/users/user.service";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"],
})
export class ProfilePage implements OnInit {
  public userId = window.localStorage.getItem("userId");
  public userData: any = {};
  constructor(
    public loadingService: LoadingService,
    public userService: UserService,
    public alertService: AlertService,
    private alertController: AlertController,
    public router: Router
  ) { }

  ngOnInit() {
    this.getProfile();
  }

  getProfile() {
    this.loadingService.presentLoading();
    this.userService.getUserById(this.userId).subscribe(
      (data: any) => {
        this.userData = data;
        setTimeout(() => {
          this.loadingService.dismiss();
        }, 400);
      },
      (error) => {
        setTimeout(() => {
          this.loadingService.dismiss();
        }, 300);
        this.alertService.presentAlert("", error.message.message ? error.message.message : 'Something went wrong');
      }
    );
  }

  async onlogOutClick() {
    const alert = await this.alertController.create({
      subHeader: "Are you sure you want to quit the app?",
      buttons: [
        {
          text: "No",
          role: "cancel",
          handler: () => { },
        },
        {
          text: "Yes",
          role: "success",
          handler: () => {
            this.logOut();
          },
        },
      ],
    });
    await alert.present()
  }

  async logOut() {
    await this.loadingService.presentLoading();
    this.userService.updateUser(this.userData).subscribe(
      async () => {
        setTimeout(() => {
          this.loadingService.dismiss();
        }, 200);
        window.localStorage.clear();
        this.router.navigate(["/login"]);
        window.location.reload();
      },
      async () => {
        setTimeout(() => {
          this.loadingService.dismiss();
        }, 200);
        this.alertService.presentAlert("", "Error while logging out");
      }
    );
  }
}
