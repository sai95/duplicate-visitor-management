import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config, getApi } from 'src/app/config';

@Injectable({
  providedIn: 'root'
})
export class VisitorService {

  constructor(
    private http: HttpClient
  ) { }

  getVisitorWithOTP(searchTerm: any): Observable<any> {
    return this.http.get(`${getApi()}/api/visitor/suggest?q=${searchTerm}`)
  }

  createVisitor(data: any): Observable<any> {
    return this.http.post(`${getApi()}/api/visitor-log`, data)
  }

  updateVisitor(data:any):Observable<any>{
    return this.http.put(`${getApi()}/api/visitor-log/${data.id}`, data)
  }

  getVisitor(id: any): Observable<any>{
    return this.http.get(`${getApi()}/api/visitor-log/${id}`)
  }
}
