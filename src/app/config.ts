export const config = {
    // apiUrl: 'http://192.168.1.49:9000',
    // apiUrl: 'https://alpha.thehousemonk.com',
    // apiUrl: 'https://cove-dev.thehousemonk.com',
    // apiUrl: 'https://staging.thehousemonk.com',
     apiUrl: 'https://dashboard.thehousemonk.com',
}

export const getApi = () => {

    const baseURL = window.localStorage.getItem('baseURL');

    return baseURL || config.apiUrl
}