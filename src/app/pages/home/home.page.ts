import { Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, Gesture, GestureController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert.service';
import { CameraService } from 'src/app/services/camera/camera.service';
import { ColorService } from 'src/app/services/color/color.service';
import { LoadingService } from 'src/app/services/loading.service';
import { LoginService } from 'src/app/services/login.service';
import * as moment from "moment"
import 'moment-timezone';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild('card', { read: ElementRef })
  card!: ElementRef<HTMLIonCardElement>;
  private lastOnStart: number = 0;
  private DOUBLE_CLICK_THRESHOLD: number = 500;
  public purpose: string = 'guest';
  public isRedIcon: boolean = true;
  public orgLogo: any;
  public loading: boolean = true;
  public Date  = new Date().toISOString();
  constructor(
    private router: Router,
    private loginService: LoginService,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private colorService: ColorService,
    private platform: Platform,
    public gestureCtrl: GestureController,
    private ngZone: NgZone
  ) {
    this.platform.ready().then(async () => {
      this.orgLogo = window.localStorage.getItem('orgLogo');

    })
  }

  ngOnInit() {
    setInterval(()=>{
      this.Date  =  new Date().toISOString();
    }, 1000);
  }

  ngAfterViewInit() {
    const gesture = this.gestureCtrl.create({
      el: this.card.nativeElement,
      threshold: 0,
      gestureName: 'double-click',
      onStart: () => this.logPress()
    });

    gesture.enable();
  }

  logPress(){
    const now = Date.now();
    if (Math.abs(now - this.lastOnStart) <= this.DOUBLE_CLICK_THRESHOLD) {
      this.ngZone.run(() => {
        this.router.navigate(['/profile']);
      });
      this.lastOnStart = 0;
    } else {
      this.lastOnStart = now;
    }
  }
  async ionViewDidEnter() {
    this.loading === true ? await this.loadingService.presentLoading() : '';
    setTimeout(() => {
      this.refreshToken();
    }, 500);
  }

  public async refreshToken(): Promise<any> {
    this.loginService.newRentalsRefreshToken().subscribe((data: any) => {
      this.loadingService.dismiss();
      this.loading = false;
      console.log(data);
      this.setValues(data);
    }, (err) => {
      this.loadingService.dismiss();
      if(err.error.message.includes('token invalid')){
        window.localStorage.clear();
        this.router.navigate(['/login'])
      }else{
        this.alertService.presentAlert('', "Something went wrong")
      }
    });
  }

  async setValues(data: any) {
    window.localStorage.setItem('isLoggedIn', 'true');
    window.localStorage.setItem('userId', data.uid);
    window.localStorage.setItem('token', data.token);
    window.localStorage.setItem('organization', data.organization);
    window.localStorage.setItem('userRole', data.role?._id);
    window.localStorage.setItem('organizationTimezone', data.organizationTimezone ? data.organizationTimezone : Intl.DateTimeFormat().resolvedOptions().timeZone);
    if(data?.visitorManagementAppTheme){
      window.localStorage.setItem('primaryColor', data.visitorManagementAppTheme?.primaryColor || "#ffbc00" );
    }
    if(data.visitorManagementAppTheme){
      window.localStorage.setItem('secondaryColor', data.visitorManagementAppTheme?.secondaryColor || "#7e1946");
    }
    window.localStorage.setItem('orgLogo', data.visitorManagementAppTheme?.logo);
    this.colorService.setPrimaryColor();
    this.colorService.setSecondaryColor();
    this.orgLogo = window.localStorage.getItem('orgLogo');

    this.router.navigate(['/home'])
  }


  selectPurpose(purpose: string) {
    this.purpose = purpose;
    // this.router.navigate(['/login'])
  }
  navigateToCheckIn() {
    this.router.navigate(['/self-checkin'], {
      queryParams: {
        visitorType: this.purpose
      }
    })
    // console.log("PURPOSE", this.purpose)

  }
  navigateToProfile(){
    this.router.navigate(['/profile'])
  }
}
